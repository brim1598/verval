# Java Unit Testing with JUnit *- I. rész*


## Laborfeladat
> **Laborhoz szükséges:** Java, Maven, IDE *(pl: Eclipse, IntelliJ, egyéb)*
> 
> **JUnit Laborházi első része:**
> - I. rész:
>   - 5 darab unit teszt, a következők használata:
>     - assertek: assertEquals, assertTrue, assertArrayEquals - [dok](https://junit.org/junit5/docs/5.0.1/api/org/junit/jupiter/api/Assertions.html)
>     - @BeforeAll vagy @BeforeEach
>     - paraméterezett teszt
>  - II. rész:
>     - tesztek kibővítése
>       - mocks, stubs, spies - hamarosan
>       
> **HATÁRIDŐ:** csoportodnak megfelelő **3. labor** a házi első és második része együtt

## Bevezető

**Követelmények ismertetése**
- labortémák, pontozás, jelenlét
  - a hivatalos announcementet Barabás L. publikálja

**Unit testing fogalma:**
- a fejlesztés része, White Box testing
- a forrás egy részének a helyességét tesztelni
- háromszöges példa - mire vagyunk ilyenkor kiváncsiak?
  - határesetek
  - negatív számok
  - különböző típusok
  - általános helyes bemenetre elvárt eredmény
- tesztek függetlensége
- Covarage (lefedettség)

**Könyvtárszerkezet:**

- src/main/java - for Java classes
- src/test/java - for test classes

**Elnevezési konvenciók:**

Tesztosztály neve: *teszteltOsztályNeve*+Test.java
 - src/main/java/BusinessLogic.java
 - src/test/java/BusinessLogicTest.java

Teszt neve legyen minél beszédesebb.
Szokás használni a *should* kulcsszót
```java
public class BusinessLogicTest {

  @Test
  public void orderShouldBeFilled() {
  }
}
```

Másik tipikus teszt elnevezési mód: *Given[ExplainYourInput]When[WhatIsDone]Then[ExpectedResult]*

**Annotációk:**

- @Test - a metódust teszt metódusként azonosítja
- @BeforeAll és @BeforeEach
- @AfterAll and @AfterEach
- @Disable - az adott teszt nem fut le
- stb.

**Assert-ek:**

- assertTrue([üzenet,] boolean feltétel)
- assertEquals([üzenet,] elvárt, aktuális)
- assertNull([üzenet,] objektum)
- stb.

**Paraméterezett tesztek**
- ugyanazon teszt sokszoros futtatása különböző paraméterekkel -> Külön teszt esetek
- @ParameterizedTest annotáció @Test helyett
- @ValueSource -> egy paraméter tesztenként, primitív típusok

***