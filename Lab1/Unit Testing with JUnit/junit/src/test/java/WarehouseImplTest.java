import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.*;

public class WarehouseImplTest {

    private WarehouseImpl warehouse = new WarehouseImpl();

    @BeforeEach
    public void initAddTest() {
        warehouse.add("string1",1);
        warehouse.add("string2",2);
        warehouse.add("string3",3);

    }

    @Test
    public void hasInvetorytest(){
        Assertions.assertTrue(warehouse.hasInventory("string1",1));
    }

    @Test
    public void getInventorytest(){
        Assertions.assertEquals(2,warehouse.getInventory("string2"));
    }

    @Test
    public void assertArrayEqualsTest(){
        HashMap<String,Integer> expectedOutput = new HashMap<String, Integer>();

        expectedOutput.put("string1",1);
        expectedOutput.put("string2",2);
        expectedOutput.put("string3",3);

        Set<String> methodOutput = warehouse.getProducts();


        Assertions.assertArrayEquals(expectedOutput.keySet().toArray(), methodOutput.toArray());
    }

    @ParameterizedTest
    @CsvSource({"string1,1"})
    public void removeInventoryTest(String string,int asd){
        HashMap<String,Integer> expectedOutput = new HashMap<String, Integer>();

        expectedOutput.put("string1",1);
        expectedOutput.put("string2",1);
        expectedOutput.put("string3",3);

        Set<String> methodOutput = warehouse.getProducts();

        Assertions.assertArrayEquals(expectedOutput.keySet().toArray(), methodOutput.toArray());
    }
}
