import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class OrderTest {
	
	private String name = "csoki";
	private int quantity = 3;
	private Order order;
	
    @Mock
    Warehouse warehouseMock;
    
    @Spy
    WarehouseImpl warehouseSpy;
    
    @Captor
	ArgumentCaptor<String> argCaptor;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        order = new Order(name, quantity);
    }

    @Test
    public void fulfillNotEnoughDontCallRemove() {
        when(warehouseMock.hasInventory(name, quantity)).thenReturn(false);
        order.fulfill(warehouseMock);
        verify(warehouseMock, times(0)).remove(anyString(), anyInt());
        assertFalse(order.fulfilled);
    }
    
    @Test
    public void fulfillEnoughCallRemove() {
        when(warehouseMock.hasInventory(name, quantity)).thenReturn(true);
        order.fulfill(warehouseMock);
        verify(warehouseMock, times(1)).remove(anyString(), anyInt());
        assertTrue(order.fulfilled);
    }
    
    @Test
    public void fulfillAndGetPreviousQuantityCheckIfAddsQuantity() {
        Mockito.doReturn(quantity).when(warehouseSpy).getInventory(name);
        
        int expected = 2 * quantity;
        assertEquals(expected, order.fulfillAndGetPreviousQuantity(warehouseSpy));
    }
    
    @Test
    public void testIfFulFillCallsGetInventory() {
        order.fulfill(warehouseSpy);
		Mockito.verify(warehouseSpy).getInventory(argCaptor.capture());
		
		assertEquals(name, argCaptor.getValue());
    }
}
