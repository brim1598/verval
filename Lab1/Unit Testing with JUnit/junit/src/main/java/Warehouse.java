import java.util.Set;

public interface Warehouse {

    public int getInventory(String productName);

    public void add(String productname, int quantity);

    public boolean hasInventory(String productname, int quantity);

    public void remove(String productname, int quantity);

    public Set<String> getProducts();

}