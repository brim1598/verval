public class Order {

    private String productName;
    private int quantity;
    boolean fulfilled;

    public Order(String productName, int quantity) {
        this.productName = productName;
        this.quantity = quantity;
        this.fulfilled = false;
    }

    public void fulfill(Warehouse warehouse) {
        if (warehouse.hasInventory(productName, quantity)) {
            warehouse.remove(productName, quantity);
            fulfilled = true;
        }
    }
    
    public int fulfillAndGetPreviousQuantity(Warehouse warehouse) {
    	fulfill(warehouse);
    	
    	if(fulfilled) {
    		return warehouse.getInventory(productName) + getQuantity();
    	}
    	else {
    		return warehouse.getInventory(productName);
    	}
    }

    public boolean isFulfilled() {
        return fulfilled;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}