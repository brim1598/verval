package POM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Product {
	
	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;
		
	public Product(WebDriver driver, int id) {
		this.driver = driver;
		
		wait = new WebDriverWait(driver, 10);
		action = new Actions(driver);
	}
	
	public void changeColor(String colorName) {
		List<WebElement> newColor = driver.findElements(By.xpath("//a[@name='" + colorName + "']"));
		
		if (newColor.size() > 0) {
			newColor.get(0).click();
		} else {
			System.out.println(colorName + " is not an available color");
			System.exit(5);
		}
	}
	
	private final String addToCartButtonId = "add_to_cart";
	private final String secondButton = "button[1]";
	private final String continueShoppingButtonTitle = "Continue shopping";
	private final String layerCartId = "layer_cart";
	
	public void addToCart() {
		WebElement parapgraph = driver.findElement(By.id(addToCartButtonId));
		WebElement addToCartButon = parapgraph.findElement(By.xpath(secondButton));
		addToCartButon.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='" + continueShoppingButtonTitle + "']")));
		
		driver.findElement(By.xpath("//*[@title='" + continueShoppingButtonTitle + "']")).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(layerCartId)));
	}
	
	private final String cartButtonTitle = "View my shopping cart";
	private final String checkOutButtonTitle = "Check out";
	private final String cartSummaryLabelId = "cart_summary";
	
	public Order checkout() {
		WebElement cartButton = driver.findElement(By.xpath("//a[@title='" + cartButtonTitle + "']"));
		
		action.moveToElement(cartButton).perform();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='" + checkOutButtonTitle +"']")));
		action.moveToElement(driver.findElement(By.xpath("//a[@title='" + checkOutButtonTitle + "']"))).click().perform();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(cartSummaryLabelId)));
		return new Order(driver);
	}
	
	private final String addQunatityClass = "product_quantity_up";
	
	public void addQuantity() {
		driver.findElement(By.className(addQunatityClass)).click();
	}
	
	private final String setSizeSelectId = "group_1";
	
	public void setSize(String newSize) {
		WebElement selectElement = driver.findElement(By.id(setSizeSelectId));
		Select select = new Select(selectElement);
		select.selectByVisibleText(newSize);
	}
	
	public String getSize() {
		WebElement selectElement = driver.findElement(By.id(setSizeSelectId));
		Select select = new Select(selectElement);
		return select.getFirstSelectedOption().getText();
	}
	
	private final String cartBlockRemoveLinkClass = "ajax_cart_block_remove_link";
	
	public void removeProductFromOrder() {
		WebElement cartButton = driver.findElement(By.xpath("//a[@title='" + cartButtonTitle + "']"));
		
		action.moveToElement(cartButton).perform();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='" + checkOutButtonTitle + "']")));
		action.moveToElement(driver.findElement(By.className(cartBlockRemoveLinkClass))).click().perform();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(cartBlockRemoveLinkClass)));
	}
	
	private final String wishListButtonId = "wishlist_button";
	private final String wishListFancyBoxCloseClass = "fancybox-close";
	private final String wishListFancyBoxDesktopClass = "fancybox-desktop";

	public void addToWishList() {
		driver.findElement(By.id(wishListButtonId)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(wishListFancyBoxCloseClass)));

		driver.findElement(By.className(wishListFancyBoxCloseClass)).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(wishListFancyBoxDesktopClass)));
	}
}
