package POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	
	private WebDriver driver;
	private WebDriverWait wait;
		
	public HomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 10);
		
		driver.navigate().to("http://automationpractice.com");
	}
	
	public Shop initializeShop() {
		return new Shop(driver);
	}
	
	private final String signInButtonTitle = "Log in to your customer account";
	private final String submitLoginButtonID = "SubmitLogin";
	
	public Authentication clickOnSignIn() {
		driver.findElement(By.xpath("//*[@title='" + signInButtonTitle + "']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(submitLoginButtonID)));
		return new Authentication(driver);
	}
}
