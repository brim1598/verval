Feature: Test3

  Scenario: Test3
    Given I am on the web store
    When I make an account
    And I open the shop from authentication
    And I set subcategory to "Tops"
    And I set subsubcategory to "Blouses"
    And I open product page with id "2"
    And I change product to color "White"
    And I increase product quantity
    And I increase product quantity
    And I change product to size "M"
    Then I check product size with "M"
    And I add the product to the cart
    And I remove product from order
    Then I check if there are no orders