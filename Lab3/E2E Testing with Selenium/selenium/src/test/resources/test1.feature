Feature: Test1

  Scenario: Test1
    Given I am on the web store
    When I set subcategory to "Tops"
    And I add product with id "1"
    And I add product with id "2"
    And I set subcategory to "Dresses"
    And I set subsubcategory to "Evening Dresses"
    And I open product page with id "4"
    And I change product to color "Pink"
    And I add the product to the cart
    And I checkout from product page
    And I delete order item with index "1"
    And I proceed with checkout
    And I finish authentication without account
    Then I check if order was successful