Feature: Test2

  Scenario: Test2
    Given I am on the web store
    When I make an account
    And I sign out
    And I sign in
    And I open the shop from authentication
    And I set subcategory to "Tops"
    And I add product with id "1"
    And I add product with id "2"
    And I checkout from shop
    And I delete order item with index "1"
    And I finish authentication with account
    Then I check if order was successful
    Then I check if order price is around "30.15999984741211"