Feature: Test4

  Scenario: Test4
    Given I am on the web store
    When I make an account
    And I open the shop from authentication
    And I set subcategory to "Dresses"
    And I set subsubcategory to "Evening Dresses"
    And I open product page with id "4"
    And I add product to wishlist
    Then I check the number of wishlist items to be "1"