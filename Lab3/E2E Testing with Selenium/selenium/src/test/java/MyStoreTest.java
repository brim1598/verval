import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import POM.Authentication;
import POM.HomePage;
import POM.Order;
import POM.Product;
import POM.Shop;

public class MyStoreTest {

    private static WebDriver driver;
    private static HomePage homePage;
    private static Shop shop;
    private static Product product;
    private static Order order;
    private static Authentication authentication;

    @BeforeEach
    public void setupTest() {
        driver = new ChromeDriver();
        homePage = null;
        shop = null;
        order = null;
        authentication = null;
    }

    @Test
    public void exerciseOne() {
        homePage = new HomePage(driver);
        shop = homePage.initializeShop();
        shop.setCategory();
        shop.setSubCategory("Tops");

        shop.addProductToCartWithId(1);
        shop.addProductToCartWithId(2);

        shop.setSubCategory("Dresses");
        shop.setSubSubCategory("Evening Dresses");

        product = shop.getProductPageWithId(4);
        product.changeColor("Pink");
        product.addToCart();

        order = product.checkout();
        order.deleteProductFromOrder(1);
        authentication = order.proceedToChekout(false);

        authentication.authenticate();
        authentication.acceptAddresses();
        authentication.acceptTerms();
        authentication.payByBankWire();
        authentication.confirmOrder();

        String message = authentication.getFinalOrderMessage();
        assertEquals(message, "Your order on My Store is complete.");
    }

    @Test
    public void exerciseTwo() {
        homePage = new HomePage(driver);

        authentication = homePage.clickOnSignIn();
        authentication.makeNewAccount();
        authentication.signOut();
        authentication.signIn();

        shop = authentication.initializeShop();
        shop.setCategory();
        shop.setSubCategory("Tops");
        shop.addProductToCartWithId(1);
        shop.addProductToCartWithId(2);

        order = shop.checkout();
        order.deleteProductFromOrder(1);

        authentication = order.proceedToChekout(true);
        authentication.acceptAddresses();
        authentication.acceptTerms();
        authentication.payByBankWire();
        authentication.confirmOrder();

        String message = authentication.getFinalOrderMessage();
        assertEquals(message, "Your order on My Store is complete.");

        authentication.goToAccount();
        authentication.goToOrderHistory();
        float lastOrderPrice = authentication.getLastOrderPrice();
        assertEquals(lastOrderPrice, 30.15999984741211);
    }

    @Test
    public void exerciseThree() {
        homePage = new HomePage(driver);

        authentication = homePage.clickOnSignIn();
        authentication.makeNewAccount();

        shop = authentication.initializeShop();
        shop.setCategory();
        shop.setSubCategory("Tops");
        shop.setSubSubCategory("Blouses");

        product = shop.getProductPageWithId(2);
        product.changeColor("White");
        product.addQuantity();
        product.addQuantity();
        product.setSize("M");

        String currentSize = product.getSize();
        assertEquals(currentSize, "M");

        product.addToCart();
        product.removeProductFromOrder();

        authentication.goToAccount();
        authentication.goToOrderHistory();

        boolean errorMessage = authentication.checkIfNoOrders();
        assertEquals(errorMessage, true);
    }

    @Test
    public void exerciseFour() {
        homePage = new HomePage(driver);

        authentication = homePage.clickOnSignIn();
        authentication.makeNewAccount();

        shop = authentication.initializeShop();
        shop.setCategory();
        shop.setSubCategory("Dresses");
        shop.setSubSubCategory("Evening Dresses");

        product = shop.getProductPageWithId(4);
        product.addToWishList();

        authentication.goToAccount();
        authentication.goToWishLists();

        int numberOfItems = authentication.getNumberOfWishListItems();
        assertEquals(numberOfItems, 1);
    }

    @AfterAll
    public static void quitDriver() {
        driver.quit();
    }

}