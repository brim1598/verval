import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import POM.Authentication;
import POM.HomePage;
import POM.Order;
import POM.Product;
import POM.Shop;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CucumberTests {

    private static WebDriver driver;
    private static HomePage homePage;
    private static Shop shop;
    private static Product product;
    private static Order order;
    private static Authentication authentication;

    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        homePage = new HomePage(driver);
        shop = null;
        order = null;
        authentication = null;
    }

    @Given("^I am on the web store$")
    public void openHomePage() {
        shop = homePage.initializeShop();
        shop.setCategory();
    }

    @And("^I set subcategory to \"(.*)\"$")
    public void setSubcategoryTops(String subCategory) {
        shop.setSubCategory(subCategory);
    }

    @And("^I set subsubcategory to \"(.*)\"$")
    public void setSubSubcategoryTops(String subSubCategory) {
        shop.setSubSubCategory(subSubCategory);
    }

    @And("^I add product with id \"(.*)\"$")
    public void addProductWithId(String productId) {
        shop.addProductToCartWithId(Integer.parseInt(productId));
    }

    @And("^I open product page with id \"(.*)\"$")
    public void openProductPage(String productId) {
        product = shop.getProductPageWithId(Integer.parseInt(productId));
    }

    @And("^I change product to color \"(.*)\"$")
    public void changeProductColor(String color) {
        product.changeColor(color);
    }

    @And("^I add the product to the cart$")
    public void addProductToCart() {
        product.addToCart();
    }

    @And("^I checkout from product page$")
    public void checkoutFromProductPage() {
        order = product.checkout();
    }

    @And("^I delete order item with index \"(.*)\"$")
    public void deleteOrderItemWithIndex(String index) {
        order.deleteProductFromOrder(Integer.parseInt(index));
    }

    @And("^I proceed with checkout$")
    public void proceedOrderCheckout() {
        authentication = order.proceedToChekout(false);
    }

    @And("^I finish authentication without account$")
    public void finishAuthenticationWithoutAccount() {
        authentication.authenticate();
        authentication.acceptAddresses();
        authentication.acceptTerms();
        authentication.payByBankWire();
        authentication.confirmOrder();
    }

    @Then("^I check if order was successful$")
    public void checkIfOrderWasSuccessful() {
        String message = authentication.getFinalOrderMessage();
        assertEquals(message, "Your order on My Store is complete.");
    }

    @When("^I make an account$")
    public void makeAccount() {
        authentication = homePage.clickOnSignIn();
        authentication.makeNewAccount();
    }

    @And("^I sign out$")
    public void signOut() {
        authentication.signOut();
    }

    @And("^I sign in$")
    public void signIn() {
        authentication.signIn();
    }

    @And("^I open the shop from authentication$")
    public void openShopFromAuthentication() {
        shop = authentication.initializeShop();
        shop.setCategory();
    }

    @And("^I checkout from shop$")
    public void checkoutFromShop() {
        order = shop.checkout();
    }

    @And("^I finish authentication with account$")
    public void finishAuthenticationWithAccount() {
        authentication = order.proceedToChekout(true);
        authentication.acceptAddresses();
        authentication.acceptTerms();
        authentication.payByBankWire();
        authentication.confirmOrder();
    }

    @Then("^I check if order price is around \"(.*)\"$")
    public void checkIfOrderWasSuccessful(String price) {
        authentication.goToAccount();
        authentication.goToOrderHistory();
        float lastOrderPrice = authentication.getLastOrderPrice();
        assertEquals(lastOrderPrice, Float.parseFloat(price), 0.1);
    }

    @And("^I increase product quantity$")
    public void increaseProductQuantity() {
        product.addQuantity();
    }

    @And("^I change product to size \"(.*)\"$")
    public void changeProductSize(String size) {
        product.setSize(size);
    }

    @Then("^I check product size with \"(.*)\"$")
    public void checkProductSize(String size) {
        String currentSize = product.getSize();
        assertEquals(currentSize, "M");
    }

    @And("^I remove product from order$")
    public void removeProductFromOrder() {
        product.removeProductFromOrder();
    }

    @Then("^I check if there are no orders$")
    public void checkIfThereAreNoOrders() {
        authentication.goToAccount();
        authentication.goToOrderHistory();

        boolean errorMessage = authentication.checkIfNoOrders();
        assertEquals(errorMessage, true);
    }

    @And("^I add product to wishlist$")
    public void addProductToWishlist() {
        product.addToWishList();
    }

    @Then("^I check the number of wishlist items to be \"(.*)\"$")
    public void checkNumberOfWishlistItems(String noOfItems) {
        authentication.goToAccount();
        authentication.goToWishLists();

        int numberOfItems = authentication.getNumberOfWishListItems();
        assertEquals(numberOfItems, Integer.parseInt(noOfItems));
    }

    @After
    public void quitDriver() {
        driver.quit();
    }
}
