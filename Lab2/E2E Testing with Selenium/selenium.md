# E2E Testing with Selenium


## Laborfeladat
> **Laborhoz szükséges:** Java, Maven, IDE *(pl: Eclipse, IntelliJ, egyéb)*, Webdriver *(pl: chromedriver, geckodriver)*
> 
> **Vásároljunk! :)**
> My Store - http://automationpractice.com - E2E tesztek gyakorlására készült, webáruházat szimuláló oldalra írjunk automatizált teszteket
> 1. feladat:
>     - Nyissuk meg az oldalt
>     - Tegyünk a kosárba 2 felsőt és egy estélyi ruhát az oldalon található szűrőkkel. Egyiknek változtassuk meg a színét az alapértelmezettről
>     - Menjünk a checkout-ra
>     - A summary tab-en a felsők mennyiségét csökkentsük 1-re
>     - Nyomjuk meg a proceed to checkout-ot
>     - Töltsük ki a regisztrációs mezőket
>     - Fogadjuk el a szállítási és számlázási címet
>     - A shipping tab-on fogadjuk el a feltételeket
>     - Paymentnél válasszunk egy fizetési módot s erősítsük meg a rendelésünket
>     - Ellenőrizzük, hogy az lett-e kiírva, hogy "Your order on My Store is complete."
>  
> 2. feladat:
>     - Nyissuk meg az oldalt
>     - Kattintsunk a Sign in-re
>     - Hozzunk létre új fiókot
>     - Jelentkezzünk ki
>     - Jelentkezzünk be az előbb létrehozott fiókkal
>     - Tegyünk be a kosárba 2 különböző terméket
>     - Menjünk a check-outra
>     - Töröljük az egyik terméket
>     - Nyomjuk meg a proceed to checkoutot
>     - Fogadjuk el a szállítási és számlázási címet
>     - A shipping tab-en fogadjuk el a feltételeket
>     - Paymentnél válasszunk egy fizetési módot s erősítsük meg a rendelésünket
>     - Ellenőrizzük, hogy az lett-e kiírva, hogy "Your order on My Store is complete."
>     - Kattintsunk a felhasználónevünkre
>     - Kattintsunk az Order history and details
>     - Ellenőrizzük le hogy legutóbbi rendelés ára megfelelő e
>     
> 3. feladat:
>     - Találj ki **két** hasonló bonyolultságú használati esetet
> 
> **PONTOZÁS:** összesen **10** pont, ebből
> - **6** pont: működő tesztek
> - **2** pont: page objekt modellek létrehozása
> - **2** pont: nem hardkódolt másodperc várakozás, hanem valamilyen web element-re várakozás  
> 
> **HATÁRIDŐ:** csoportodnak megfelelő **5. labor**

### E2E tesztelés

A szoftver tesztelése a valós alkalmazásához hasonlóan. A cél egy-egy átlagos felhasználói viselkedést automatizálni. Ezáltal a szoftver minden része érintett lesz a teszt által, minden komponense használva lesz. 


### Selenium 

*"Selenium automates browsers. That's it! What you do with that power is entirely up to you. Primarily, it is for automating web applications for testing purposes, but is certainly not limited to just that. Boring web-based administration tasks can (and should!) be automated as well."*

A keretrendszer biztosít számunkra metódusokat arra, hogy irányítsuk a böngészőt, végfelhasználóként kezeljük, például:
  * get(url)
  * WebElement.click()  

Valamint olyan függvényeket állít rendelkezésünkre, melyekkel (egyértelműen) meghatározhatjuk a programunk számára a weboldal elemeit, például: 
  * findElement(By.id("nameInput"))

##### Selector-ok

Utóbbi az egyik kihívása az automata tesztek írásának: fontos, hogy minimális változtatások kelljenek ha a weboldalba új funkcionalitások, elemek kerülnek fejlesztés közben. Egyedi módon kell megtalálnunk az elemeket, úgy hogy minél kevesebb más elemtől függjön a selectorunk.
A legjobb selector az id-n alapuló, hiszen egy helyes html oldalon minden id-nak egyedinek kell lennie, viszont a dinamikusan létrehozott elemeknek még ha lesz is generált egyedi id-juk, tesztjeinkben valószínűleg nem lesz miből kikövetkeztetnünk őket. Ilyenkor támaszkodhatunk a name attributumára, az osztály (class) nevére, valamint szülő és/vagy gyerek nódusaira. Az ilyen jellegű kiválasztáshoz ajánljuk az **XPath** használatát.

*Például*: 
`//div[contains(@class,'shopping_cart')]/a`  
`div` keresése bárhol a html dokumentumban a `//` miatt. A `[contains(@class,'shopping_cart')]` kifejezés miatt csak olyan div-ekre fog illeszkedni a kifejezés, amelyekre igaz, hogy az osztály nevük tartalmazza a 'shopping_cart' karakterláncot. A selector az ezen div-ek közvetlen (a `/` miatt) gyerek nódusaira fognak illeszkedni, amelyek a típusú nódusok.
Segítséget nyújthat az XPathek meghatározásában különböző böngésző extension (pl. Chropath), viszont a böngészők **inspect** funkcionalitását is használhatjuk: jobb-click egy nódusra, majd a copy alatt a "Copy XPath"-et válasszuk. A selectorjaink helyességét az Element inspectorban a find (ctrl+f) funkcióval is ellenőrizhetjük.
Megkülönböztetjük az abszolút és relatív XPath-eket. Az abszolút útvonal a dokumentum gyökeréből indul ki, például:
``/html/body/div[5]/div[2]/div/div[2]/div[2]/h2[1]``. Igaz hogy ez a fajta kiválasztás gyorsabb lesz, viszont bármilyen apró változás a DOM-ban invaliddá teheti. A relatív XPath-ek általunk kinevezett elemekből indulnak ki bárhonnan a dokumentumban. Minél kevesebb elemtől függ a selectorunk, az annál biztosabb lesz.



##### Wait-ek

A tesztjeink utasításai gyakran gyorsabban futnak le mint ahogyan a weboldal reagálna a "user" inputjainkra. Például attól, hogy kiadtunk egy parancsot egy dialog megnyitására, a következő utasításunk nem lehet az azon a dialogon levő elem kattintása, hiszen ez kivételhez vezet (No Such Element Exception). Ilyenkor várnunk kell.

Az egyik lehetőségünk az **implicit wait**: ha tudjuk, hogy a DOM újrarenderelődik, várunk néhány másodpercet, például:
`driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);`

A *jobbik* megoldás az **explicit wait**, ami abból áll, hogy adott feltétel teljesítésére várunk (elem kattinthatósága, láthatósága, -lásd ExpectedConditions), például:
``WebDriverWait wait = new WebDriverWait(driver, 10);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("someid")));
``


##### POM (Page Object Model)

A POM egy tervezési minta, melynek célja a könnyen karbantartható, kódismétlést mellőző webalkalmazás tesztek írásának segítése. 
Az alapelv, hogy az oldalt leíró selectorok s az oldalon elvégzendő műveletek el kell legyenek választva a tesztesetektől. Ezáltal egy helyen deklaráljuk ugyanannak az elemnek a selectorját s változás esetén csak egy helyen kell megváltoztatnunk.
A gyakorlatban ez azt jelenti, hogy lesznek külön osztályaink különböző oldalakra: pl: HomePage, AccountPage de még MenuBarComponentünk is lehetne. A MenuBarComponent-ben deklarálhatnánk a menüpontok selectorjait (women, dresses, t-shirts) valamint metódusokat ezekkel kapcsolatban (pl: changeToDresses()). 