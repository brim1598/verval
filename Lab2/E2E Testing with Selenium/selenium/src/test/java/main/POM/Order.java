package main.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Order {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	public Order(WebDriver driver) {
		this.driver = driver;
		
		wait = new WebDriverWait(driver, 10);
	}
	
	private final String cartSummaryButtonId = "cart_summary";
	private final String firstTbody = "tbody[1]";
	private final String cartDeleteButtonClass = "cart_delete";
	private final String firstDiv = "div[1]";
	private final String firstA = "a[1]";
	
	public void deleteProductFromOrder(int index) {
		WebElement table = driver.findElement(By.id(cartSummaryButtonId));
		WebElement tbody = table.findElement(By.xpath(firstTbody));
		WebElement product = tbody.findElement(By.xpath("tr[" + index + "]"));
		WebElement deleteCell = product.findElement(By.className(cartDeleteButtonClass));
		WebElement randomDiv = deleteCell.findElement(By.xpath(firstDiv));
		WebElement deleteButton = randomDiv.findElement(By.xpath(firstA));
		deleteButton.click();
		wait.until(ExpectedConditions.invisibilityOf(deleteButton));
	}
	
	private final String cartNavigationParagraphClass = "cart_navigation";
	private final String cartNavigationButtonClass = "button";
	private final String processAddressButtonName = "processAddress";
	private final String submitLignButtonId = "SubmitLogin";
	
	public Authentication proceedToChekout(boolean loggedIn) {
		WebElement parapgraph = driver.findElement(By.className(cartNavigationParagraphClass));
		parapgraph.findElement(By.className(cartNavigationButtonClass)).click();
		
		if (loggedIn) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name='" + processAddressButtonName + "']")));
		} else {
			wait.until(ExpectedConditions.elementToBeClickable(By.id(submitLignButtonId)));
		}
		
		return new Authentication(driver);
	}
}
