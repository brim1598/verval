package main.POM;

import main.Tools.RandomString;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Authentication {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	private String accountEmail;
	private String accountPassword;
	
	private final String emailCreateTextInputID = "email_create";
	private final String submitEmailButtonID = "SubmitCreate";
	private final String accountCreationFormID = "account-creation_form";
	
	private final String customerFirstNameTextInputID = "customer_firstname";
	private final String customerLastNameTextInputID = "customer_lastname";
	private final String customerPasswordTextInputID = "passwd";
	private final String firstNameTextInputID = "firstname";
	private final String lastNameTextInputID = "lastname";
	private final String addressTextInputID = "address1";
	private final String cityTextInputID = "city";
	private final String postCodeTextInputID = "postcode";
	private final String phoneNumberTextInputID = "phone_mobile";
	private final String aliasTextInputID = "alias";
	private final String stateCheckboxID = "id_state";
	private final String submitAccountButtonID = "submitAccount";
	
	private final String orderMessageID = "ordermsg";
	private final String centerColumnOnOrderID = "center_column";
	private final String processAddressTextName = "processAddress";
	private final String acceptAddressTextID = "HOOK_BEFORECARRIER";
	private final String processTermsButtonID = "cgv";
	private final String processCarrierID = "processCarrier";
	private final String cartSummaryID = "cart_summary";
	
	private final String bankWireButtonID = "Pay by bank wire";
	private final String checkBoxClass = "cheque-box";
	
	private final String cartNavigationID = "cart_navigation";
	private final String confirmOrderButtonID = "submit";
	private final String confirmOrderProgressButtonClass = "box";
	
	private final String finalOrderParagraphClass = "cheque-indent";
	private final String finalOrderMessageClass = "dark";
	
	public Authentication(WebDriver driver) {
		this.driver = driver;
		
		wait = new WebDriverWait(driver, 10);
	}
	
	public void authenticate() {
		addEmailAddress();
		addRest(true);
	}
	
	public void makeNewAccount() {
		addEmailAddress();
		addRest(false);
	}
	
	private void addEmailAddress() {
		WebElement inputToFill = driver.findElement(By.id(emailCreateTextInputID));
		
		String newEmail = RandomString.randomLetters(5) + "@" + RandomString.randomLetters(5) + ".com";
		inputToFill.sendKeys(newEmail);
		this.accountEmail = newEmail;
		
		driver.findElement(By.id(submitEmailButtonID)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(accountCreationFormID)));
	}
	
	private void addRest(boolean order) {
		driver.findElement(By.id(customerFirstNameTextInputID)).sendKeys(RandomString.randomLetters(5));
		driver.findElement(By.id(customerLastNameTextInputID)).sendKeys(RandomString.randomLetters(5));
		this.accountPassword = RandomString.randomLetters(5);
		driver.findElement(By.id(customerPasswordTextInputID)).sendKeys(this.accountPassword);
		driver.findElement(By.id(firstNameTextInputID)).sendKeys(RandomString.randomLetters(5));
		driver.findElement(By.id(lastNameTextInputID)).sendKeys(RandomString.randomLetters(5));
		driver.findElement(By.id(addressTextInputID)).sendKeys(RandomString.randomLetters(5));
		driver.findElement(By.id(cityTextInputID)).sendKeys(RandomString.randomLetters(5));
		driver.findElement(By.id(postCodeTextInputID)).sendKeys(RandomString.randomNumbers(5));
		driver.findElement(By.id(phoneNumberTextInputID)).sendKeys(RandomString.randomNumbers(10));
		driver.findElement(By.id(aliasTextInputID)).sendKeys(RandomString.randomLetters(5));
		
		WebElement selectElement = driver.findElement(By.id(stateCheckboxID));
		Select select = new Select(selectElement);
		select.selectByIndex(1);
		
		driver.findElement(By.id(submitAccountButtonID)).click();
		
		if (order) {
			waitOrder();
		} else {
			waitAccount();
		}
	}
	
	private void waitOrder() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(orderMessageID)));
	}
	
	private void waitAccount() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(centerColumnOnOrderID)));
	}
	
	public void acceptAddresses() {
		driver.findElement(By.xpath("//*[@name='" + processAddressTextName + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(acceptAddressTextID)));
	}
	
	public void acceptTerms() {
		driver.findElement(By.id(processTermsButtonID)).click();
		driver.findElement(By.xpath("//*[@name='" + processCarrierID + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(cartSummaryID)));
	}
	
	public void payByBankWire() {
		driver.findElement(By.xpath("//*[@title='" + bankWireButtonID + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(checkBoxClass)));
	}
	
	public void confirmOrder() {
		WebElement panel = driver.findElement(By.id(cartNavigationID));
		panel.findElement(By.xpath("button[@type='" + confirmOrderButtonID + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(confirmOrderProgressButtonClass)));
	}
	
	public String getFinalOrderMessage() {
		WebElement paragraph = driver.findElement(By.className(finalOrderParagraphClass));
		WebElement strong = paragraph.findElement(By.className(finalOrderMessageClass));
		return strong.getText();
	}
	
	private final String logOutButtonTitle = "Log me out";
	private final String submitLoginButtonID = "SubmitLogin";
	
	public void signOut() {
		driver.findElement(By.xpath("//*[@title='" + logOutButtonTitle + "']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(submitLoginButtonID)));
	}
	
	private final String accountEmailTextInputID = "email";
	private final String accountPasswordTextInputID = "passwd";
	
	public void signIn() {
		driver.findElement(By.id(accountEmailTextInputID)).sendKeys(this.accountEmail);
		driver.findElement(By.id(accountPasswordTextInputID)).sendKeys(this.accountPassword);
		
		driver.findElement(By.id(submitLoginButtonID)).click();
		waitAccount();
	}
	
	public Shop initializeShop() {
		return new Shop(driver);
	}
	
	private final String accountLinkText = "View my customer account";
	
	public void goToAccount() {
		driver.findElement(By.xpath("//a[@title='" + accountLinkText + "']")).click();
		waitAccount();
	}
	
	private final String orderHistoryButtonTitle = "Orders";
	private final String footerLinkClass = "footer_links";
	
	public void goToOrderHistory() {
		driver.findElement(By.xpath("//a[@title='" + orderHistoryButtonTitle + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(footerLinkClass)));
	}
	
	private final String orderTableId = "order-list";
	private final String secondTableElement = "tbody[1]";
	private final String firstRowTableClass = "first_item";
	private final String priceCellClass = "history_price";
	private final String priceValueAttribute = "data-value";
	
	public float getLastOrderPrice() {
		WebElement table = driver.findElement(By.id(orderTableId));
		WebElement tbody = table.findElement(By.xpath(secondTableElement));
		WebElement lastOrder = tbody.findElement(By.className(firstRowTableClass));
		WebElement priceCell = lastOrder.findElement(By.className(priceCellClass));
		return Float.parseFloat(priceCell.getAttribute(priceValueAttribute));
	}
	
	private final String alertWarningClass = "alert-warning";
	
	public boolean checkIfNoOrders() {
		WebElement alert = driver.findElement(By.className(alertWarningClass));
		return alert.isDisplayed();
	}
	
	private final String goToWishListTitle = "My wishlists";
	private final String historyButtonId = "block-history";
	
	public void goToWishLists() {
		driver.findElement(By.xpath("//a[@title='" + goToWishListTitle + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(historyButtonId)));
	}
	
	public void checkWishList() {
		driver.findElement(By.id(historyButtonId));
	}
	
	private final String firstTable = "table[1]";
	private final String firstTr = "tr[1]";
	private final String firstTd = "td[1]";
	private final String firstA = "a[1]";
	private final String sendWishListButtonID = "showSendWishlist";
	private final String quantityCellID = "quantity_4_16";
	
	public int getNumberOfWishListItems() {
		WebElement div = driver.findElement(By.id(historyButtonId));
		WebElement table = div.findElement(By.xpath(firstTable));
		WebElement tbody = table.findElement(By.xpath(secondTableElement));
		WebElement tr = tbody.findElement(By.xpath(firstTr));
		WebElement td = tr.findElement(By.xpath(firstTd));
		WebElement link = td.findElement(By.xpath(firstA));
		
		link.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(sendWishListButtonID)));

		return Integer.parseInt(driver.findElement(By.id(quantityCellID)).getAttribute("value"));
	}
}
