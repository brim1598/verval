package main.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Shop {
	
	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;
	
	private String subCategory;
	private String subSubCategory;
	
	public Shop(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 10);
		action = new Actions(driver);
	}
	
	private final String setWomenCategoryLinkText = "Women";
	private final String categoryLabelClass = "category-name";
	
	public void setCategory() {
		this.subCategory = null;
		this.subSubCategory = null;
		
		driver.findElement(By.linkText(setWomenCategoryLinkText)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className(categoryLabelClass)));
	}
	
	private final String setTopsSubCategoryLinkText = "Tops";
	private final String setDressesSubCategoryLinkText = "Dresses";
	private final String setEveningDressesSubSubCategoryLinkText = "Evening Dresses";
	
	private final String cartButtonTitle = "View my shopping cart";
	private final String checkOutButtonTitle = "Check out";
	private final String cartSummaryLabelId = "cart_summary";
	
	public void setSubCategory(String subCategory) {
		if (this.subCategory != null) {
			this.setCategory();
		}
		
		this.subCategory = subCategory;
		this.subSubCategory = null;
		
		switch(subCategory) {
			case "Tops": {
				driver.findElement(By.linkText(setTopsSubCategoryLinkText)).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.className(categoryLabelClass)));
				break;
			}
			case "Dresses": {
				driver.findElement(By.linkText(setDressesSubCategoryLinkText)).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.className(categoryLabelClass)));
				break;
			}
			default: {
				System.out.print("No such subcategory in shop as " + subCategory);
				System.exit(2);
			}
		}
	}
	
	private final String setTshirtsSubSubCategoryLinkText = "T-shirts";
	private final String setBlousesSubSubCategoryLinkText = "Blouses";

	
	public void setSubSubCategory(String subSubCategory) {
		if (this.subSubCategory != null) {
			this.setSubCategory(subCategory);
		}
		
		this.subSubCategory = subSubCategory;
		
		switch(subCategory) {
			case "Tops": {
				switch(subSubCategory) {
					case "T-shirts": {
						driver.findElement(By.linkText(setTshirtsSubSubCategoryLinkText)).click();
						wait.until(ExpectedConditions.elementToBeClickable(By.className(categoryLabelClass)));
						break;
					}
					case "Blouses": {
						driver.findElement(By.linkText(setBlousesSubSubCategoryLinkText)).click();
						wait.until(ExpectedConditions.elementToBeClickable(By.className(categoryLabelClass)));
						break;
					}
					default: {
						System.out.print("No such subsubcategory in shop as " + subSubCategory + " in subcategory Tops");
						System.exit(3);
					}
				}
				break;
			}
			case "Dresses": {
				switch(subSubCategory) {
					case "Casual Dresses": {
						System.out.println("navigated to casual dresses");
						break;
					}
					case "Evening Dresses": {
						driver.findElement(By.linkText(setEveningDressesSubSubCategoryLinkText)).click();
						wait.until(ExpectedConditions.elementToBeClickable(By.className(categoryLabelClass)));
						break;
					}
					case "Summer Dresses": {
						System.out.println("navigated to summer dresses");
						break;
					}
					default: {
						System.out.print("No such subsubcategory in shop as " + subSubCategory + " in subcategory Dresses");
						System.exit(4);
					}
				}
				break;
			}
		}
	}
	
	private final String continueShoppingButtonTitle = "Continue shopping";
	private final String layerCartId = "layer_cart";
	
	public void addProductToCartWithId(int id) {
		String idString = id + "";
		ByChained chain = new ByChained(By.linkText("Add to cart"),By.xpath("//*[@data-id-product='" + idString + "']"));
		
		driver.findElement(chain).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='" + continueShoppingButtonTitle + "']")));
		
		driver.findElement(By.xpath("//*[@title='" + continueShoppingButtonTitle +"']")).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(layerCartId)));
	}
	
	private final String productPageRightBlockClass = "right-block";
	private final String productPageButtonContainerClass = "button-container";
	private final String secondA = "a[2]";
	private final String pageProductBoxClass = "page-product-box";
	
	public Product getProductPageWithId(int id) {	
		WebElement rightBlock = driver.findElement(By.className(productPageRightBlockClass));
		WebElement ButtonContainer = rightBlock.findElement(By.className(productPageButtonContainerClass));
		
		WebElement moreButton = ButtonContainer.findElement(By.xpath(secondA));
		moreButton.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className(pageProductBoxClass)));
		
		return new Product(driver, id);
	}
	
	
	
	public Order checkout() {
		WebElement cartButton = driver.findElement(By.xpath("//a[@title='" + cartButtonTitle + "']"));
		
		action.moveToElement(cartButton).perform();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='" + checkOutButtonTitle + "']")));
		action.moveToElement(driver.findElement(By.xpath("//a[@title='" + checkOutButtonTitle + "']"))).click().perform();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(cartSummaryLabelId)));
		return new Order(driver);
	}
}
