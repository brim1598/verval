package main.Tools;

import java.security.SecureRandom;

public class RandomString {

	private static final String charList = "abcdefghijklmnopqrstuvwxyz";
	private static final String numList = "0123456789";
	private static SecureRandom rand = new SecureRandom();

	public static String randomLetters(int len) {
	   StringBuilder stringBuilder = new StringBuilder(len);
	   for (int i = 0; i < len; i++) {
		   stringBuilder.append(charList.charAt(rand.nextInt(charList.length())));
	   }
	   
	   return stringBuilder.toString();
	}
	
	public static String randomNumbers(int len) {
		   StringBuilder stringBuilder = new StringBuilder(len);
		   for (int i = 0; i < len; i++) {
			   stringBuilder.append(numList.charAt(rand.nextInt(numList.length())));
		   }
		   
		   return stringBuilder.toString();
		}
}
